﻿Option Strict On

Public Class Form1

    'Private WithEvents _picBox As New PictureBox
    Private _oldPicBoxLocation As Drawing.Point
    Private _image As Image
    Private _ukuranGambar As Drawing.Size
    Private _direction As enDirection = enDirection.enGakTahu
    Private _graph As Drawing.Graphics

    Private Enum enDirection
        enGakTahu = 0
        enKanan = 1
        enKiri = 2
    End Enum

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _graph = Me.CreateGraphics()

        Dim aPFN As String
        aPFN = IO.Path.Combine(My.Application.Info.DirectoryPath, "mobil.png")
        _image = Image.FromFile(aPFN)

        '_picBox.Image = _image
        '_picBox.Location = New Drawing.Point(0, 0)
        '_picBox.SizeMode = PictureBoxSizeMode.AutoSize
        '_picBox.Show()
        '_picBox.BackColor = Color.Transparent

        _ukuranGambar = _image.Size
        '_ukuranGambar.Height \= 2
        '_ukuranGambar.Width \= 2
        'Me.Controls.Add(_picBox)

    End Sub

    Private Sub Form1_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
        '_picBox.Location = Drawing.Point.Subtract(e.Location, _ukuranGambar)

        'Dim aPoint As Drawing.Point = _picBox.PointToScreen(e.Location)
        'aPoint = Me.PointToClient(aPoint)
        '_picBox.Location = Drawing.Point.Subtract(aPoint, _ukuranGambar)

        '==> rotating
        If _oldPicBoxLocation <> e.Location Then
            If e.Location.X > _oldPicBoxLocation.X Then
                If _direction = enDirection.enGakTahu OrElse _direction = enDirection.enKiri Then
                    _direction = enDirection.enKanan
                    _image.RotateFlip(RotateFlipType.RotateNoneFlipX)
                    Me.Text = "kanan"
                End If

            ElseIf e.Location.X < _oldPicBoxLocation.X Then
                If _direction = enDirection.enGakTahu OrElse _direction = enDirection.enKanan Then
                    _direction = enDirection.enKiri
                    _image.RotateFlip(RotateFlipType.RotateNoneFlipX)


                    Me.Text = "kiri"
                End If

            End If

            '==. draw line
            _graph.DrawLine(_pen, _oldPicBoxLocation, e.Location)

            _oldPicBoxLocation = e.Location
        End If
        ' Invalidate()
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        e.Graphics.DrawImage(_image, MousePosition)
        MyBase.OnPaint(e)
    End Sub

    Private _pen As New Drawing.Pen(Drawing.Brushes.Orange, 5)

    'Private Sub _picBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _picBox.MouseMove
    '    Dim aPoint As Drawing.Point = _picBox.PointToScreen(e.Location)
    '    aPoint = Me.PointToClient(aPoint)
    '    _picBox.Location = Drawing.Point.Subtract(aPoint, _ukuranGambar)

    '    '==> rotating
    '    If _oldPicBoxLocation <> _picBox.Location Then
    '        If _picBox.Location.X > _oldPicBoxLocation.X Then
    '            If _direction = enDirection.enGakTahu OrElse _direction = enDirection.enKiri Then
    '                _direction = enDirection.enKanan
    '                _image.RotateFlip(RotateFlipType.RotateNoneFlipX)
    '                Me.Text = "kanan"
    '            End If

    '        ElseIf _picBox.Location.X < _oldPicBoxLocation.X Then
    '            If _direction = enDirection.enGakTahu OrElse _direction = enDirection.enKanan Then
    '                _direction = enDirection.enKiri
    '                _image.RotateFlip(RotateFlipType.RotateNoneFlipX)


    '                Me.Text = "kiri"
    '            End If

    '        End If

    '        '==. draw line
    '        _graph.DrawLine(_pen, _oldPicBoxLocation, _picBox.Location)

    '        _oldPicBoxLocation = _picBox.Location
    '    End If






    'End Sub
End Class
