﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pb01 = New System.Windows.Forms.PictureBox()
        Me.lbl01 = New System.Windows.Forms.Label()
        CType(Me.pb01, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pb01
        '
        Me.pb01.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pb01.BackColor = System.Drawing.Color.Transparent
        Me.pb01.Location = New System.Drawing.Point(362, 134)
        Me.pb01.Name = "pb01"
        Me.pb01.Size = New System.Drawing.Size(334, 233)
        Me.pb01.TabIndex = 0
        Me.pb01.TabStop = False
        '
        'lbl01
        '
        Me.lbl01.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl01.BackColor = System.Drawing.Color.Transparent
        Me.lbl01.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl01.ForeColor = System.Drawing.Color.Yellow
        Me.lbl01.Location = New System.Drawing.Point(359, 370)
        Me.lbl01.Name = "lbl01"
        Me.lbl01.Size = New System.Drawing.Size(337, 37)
        Me.lbl01.TabIndex = 1
        Me.lbl01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(1058, 500)
        Me.Controls.Add(Me.lbl01)
        Me.Controls.Add(Me.pb01)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pb01, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pb01 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl01 As System.Windows.Forms.Label
End Class
