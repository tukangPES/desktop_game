﻿Option Strict On

Public Class Form2

    Private _jumlah As Integer = 5
    Private _dict As New List(Of clsJeah)
    Private _strLastSuaraPFN As String

    Private _suara As NAudio.Wave.WaveOut

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '==> siapkan resource

        Me.BackgroundImage = Bitmap.FromFile("bgr.jpg")
        Me.BackgroundImageLayout = ImageLayout.Zoom

        Dim aTemp As clsJeah
        Dim aString As String

        For Each aFile As String In IO.Directory.GetFiles(My.Application.Info.DirectoryPath, "*.mp3")
            aString = IO.Path.GetFileNameWithoutExtension(aFile)
            If IO.File.Exists(aString & ".png") Then
                aTemp = New clsJeah() With {.pfnGambar = aString & ".png", .pfnSuara = aString & ".mp3"}
            Else
                aTemp = New clsJeah() With {.pfnGambar = aString & ".jpg", .pfnSuara = aString & ".mp3"}
            End If

            aTemp.Visible = False
            aTemp.caption = aString
            AddHandler aTemp.VisibleChanged, AddressOf visibleCh
            _dict.Add(aTemp)
        Next





        For aInt As Integer = 1 To _jumlah
            Me.ambilSatu()
        Next
        lbl01.Text = ""
        pb01.Image = Nothing
    End Sub

    Private Sub ambilSatu()
        Dim aJeah As clsJeah
        Dim aIndex As Integer
        Dim aRandomer As New Random
        Do
            Try
                aIndex = aRandomer.Next(0, _dict.Count)
            Catch ex As Exception
                Continue Do
            End Try


            aJeah = _dict(aIndex)
        Loop Until aJeah.Visible = False
        Me.Controls.Add(aJeah)
        aJeah.Visible = True
        aJeah.BringToFront()
        With aJeah
            .doPindah()
        End With

    End Sub

    Private Sub visibleCh(ByVal sender As Object, ByVal e As System.EventArgs) '.	D:\Projects\desktop_game\desktop_game\Form2.vb	25	46	desktop_game
        With CType(sender, clsJeah)
            If .Visible = False Then
                If _suara IsNot Nothing Then
                    _suara.Stop()
                    _suara.Dispose()
                End If
                _suara = New NAudio.Wave.WaveOut
                Try
                    _strLastSuaraPFN = .pfnSuara
                    _suara.Init(New NAudio.Wave.Mp3FileReader(.pfnSuara))
                    _suara.Play()
                    pb01.Load(.pfnGambar)
                    pb01.SizeMode = PictureBoxSizeMode.Zoom
                    lbl01.Text = .caption
                Catch ex As Exception

                End Try

                Me.Controls.Remove(CType(sender, Control))
                Me.ambilSatu()
            End If
        End With

    End Sub

    Private Sub pb01_Click(sender As Object, e As EventArgs) Handles pb01.Click
        _suara.Dispose()

        _suara = New NAudio.Wave.WaveOut
        _suara.Init(New NAudio.Wave.Mp3FileReader(_strLastSuaraPFN))
        _suara.Play()
    End Sub
End Class
