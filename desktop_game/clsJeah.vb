﻿Option Strict On

Public Class clsJeah
    Inherits PictureBox

    Public Sub doPindah()
        Dim aRandomer As New Random
        Dim aEx As Integer
        Dim aYe As Integer
        Try
            aEx = (New Random).Next(0, Parent.Width - Me.Width)

            Randomize(Now.Ticks)
            Application.DoEvents()
            Threading.Thread.Sleep(10)
            Threading.Thread.SpinWait(10)
            aYe = (New Random).Next(0, Parent.Height - Me.Height)
            Me.Location = New Point(aEx, aYe)
            Me.Visible = True
        Catch ex As Exception

        End Try

    End Sub

    Public Property pfnGambar As String
        Set(ByVal value As String)
            Me.Load(value)
            Me.SizeMode = PictureBoxSizeMode.Zoom
            Me.Size = New Size(128, 128)
        End Set
        Get
            Return Me.ImageLocation
        End Get
    End Property

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        Me.Visible = False
        MyBase.OnMouseEnter(e)
    End Sub

    Public Property pfnSuara As String

    Public Property caption As String

    Public Sub New()
        Me.BorderStyle = BorderStyle.FixedSingle
        Me.Visible = False
        Me.BackColor = Color.Transparent
    End Sub

    Public Event playThisSound()
End Class